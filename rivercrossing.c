/**
 * Soubor:                 rivercrossing.c
 * Datum vytvoreni:        13. 4. 2014
 * Datum posledni upravy:  21. 4. 2014
 * Autor:                  Jan Herec, xherec00@stud.fit.vutbr.cz
 * Projekt:                River Crossing Problem, projekt cislo 2 do predmetu iOS na FIT VUT
 * Popis programu:         Program resi synchronizacni problem: River Crossing Problem
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <errno.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/stat.h>

/* Vkladame hlavickovy soubor s prototypy funkci, globalnimi/sdilenymi promennymi, strukturami, vyctovymi typy, ...
 * Samozrejmosti je take dokumentace k jednotlivym objektum */
#include "rivercrossing.h"

/* Deskriptor souboru, do ktereho budeme zapisovat akce osob */
FILE *soubor;

/* Instance vyctoveho typu obsahujici pozice osob na lodi */
PositionOfPersonOnTheBoat positionOfPersonOnTheBoat;

/* Instance struktury, ktera obsahuje sdilene promenne, jez budou mapovany do sdilene pameti */
SharedVars *sharedVars;

/**
 * Funkce main predstavuje samotne srdce programu, ve kterem zivot programu zacina a take konci
 */
int main(int argc, char** argv) {

    /* Nejdrive zpracujeme argumenty predane programu */
    ParseArgumentsPassedToProgram(argc, argv);

    /* Pokud vznikla pri zpracovani argumentu chyba, uvolnime doposud alokovane zdroje a ukoncime program s navratovym kodem 1*/
    if (occurenceOfErrorInMainProcess == true) {
        CleanUpResourcesForActualProcess();
        return 1;
    }

    /* Po zpracovani argumentu alokujeme zdroje spolecne pro vice procesu */
    AllocateCommonResources();
    /* Pokud vznikla chyba pri alokovani zdroju, uvolnime doposud alokovane zdroje a ukoncime program s navratovym kodem 2 */
    if (occurenceOfErrorInMainProcess == true) {
        CleanUpResourcesForActualProcess();
        CleanUpCommonResources();
        return 2;
    }

    /* V nasledujici funkci zacne hlavni proces generovat podprocesy (potomky)
     * a tedy zde zacina samotne reseni River Crossing Problemu */
    GenerateDescendantsOfMainProcess();
    /* Pokud pri generovani podprocesu a reseni River Crossing Problemu vznikla nejaka chyba (zrejme pri systemovem volani),
     * tak uvolnime doposud alokovane zdroje a ukoncime program s navratovym kodem 2 */
    if (*occurenceOfError == true) {
        CleanUpResourcesForActualProcess();
        CleanUpCommonResources();
        return 2;
    }

    /* Nakonec pokud vse probehlo korektne, uvolnime doposud alokovane zdroje
     * a ukoncime program s navratovym kodem 0 */
    CleanUpResourcesForActualProcess();
    CleanUpCommonResources();

    return 0;
}

void ParseArgumentsPassedToProgram(int argc, char** argv) {

    /* Pokud nebylo predano programu pozadovany pocet agrumentu, tak vyvolame chybu, kterou taky osetrime */
    if (argc != 5) {
        HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
        return;
    }

    /* V cyklu budeme zpracovavat postupne jednotlive argumenty,
     * tedy krome nulteho argumentu, ktery predstavuje nazev programu, ten totiz na nic nepotrebujeme */
    for (int orderOfArgument = 0; orderOfArgument < argc; orderOfArgument++) {
        /* V konstrukci switch budeme podle soucasne zpracovavaneho argumentu provadet urcitou akci */
        switch (orderOfArgument) {
            /* Zpracujeme prvni argument, ktery predstavuje pocet osob, ktere se budou generovat v kazde kategorii
             * Na tomto argumentu si ukazeme princip zpracovani, ktery se bude opakovat i pro ostatni argumenty */
            case 1:
                /* Nejdrive overime, jestli si s nami uzivatel (nebo testovaci skripty :-D) nehraje
                 * a predal nam celociselny argument a pokud toto neni splneno, vyvolame chybu, kterou taky osetrime
                 * Pokud format argumentu vyhovuje, je do promenne, kterou predavame odkazem jako druhy argument funkci
                 * StringToNumber() nacten vysledek konverze */
                if (StringToNumber(argv[orderOfArgument],&numberOfGeneratedPersons) == false) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                /* Krome formatu je potreba overit, jestli take hodnota argumentu splnuje potrebne podminky,
                 * v tomto pripade musi byt cislo vetsi nez nula a taky musi byt cislo delitelne beze zbytku dvema,
                 * pokud toto neni splneno, opet vyvolame chybu */
                if (numberOfGeneratedPersons <= 0 || numberOfGeneratedPersons % 2 != 0) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                break;

            /* Zpracujeme druhy argument, ktery predstavuje maximalni cas (v milisekundach)
             * po kterem se bude generovat novy hacker */
            case 2:
                if (StringToNumber(argv[orderOfArgument],&maxTimeAfterNewHackerIsGenerated) == false) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                if (maxTimeAfterNewHackerIsGenerated < 0 || maxTimeAfterNewHackerIsGenerated >= 5001) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                break;

            /* Zpracujeme treti argument, ktery predstavuje maximalni cas (v milisekundach)
             * po kterem se bude generovat novy serf */
            case 3:
                if (StringToNumber(argv[orderOfArgument],&maxTimeAfterNewSerfIsGenerated) == false) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                if (maxTimeAfterNewSerfIsGenerated < 0 || maxTimeAfterNewSerfIsGenerated >= 5001) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                break;

            /* Zpracujeme ctvrty a posledni argument, ktery predstavuje maximalni cas (v milisekundach)
             * po ktery muze trvat plavba */
            case 4:
                if (StringToNumber(argv[orderOfArgument],&maxTimeOfVoyage) == false) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                if (maxTimeOfVoyage < 0 || maxTimeOfVoyage >= 5001) {
                    HandleError(INCORRECT_FORMAT_OF_ARGUMENTS);
                    return;
                }
                break;
        }
    }

    return;
}

void HandleError(TypeOfError typeOfError) {

    /* V konstrukci switch budeme podle typu chyby, ktera nastala provadet urcitou akci */
    switch (typeOfError) {
        /* Pokud uzivatel nepredal argumenty v pozadovanem formatu */
        case INCORRECT_FORMAT_OF_ARGUMENTS:
            /* Zapiseme chybu na chybovy vystup */
            fprintf(stderr, "Zadali jste nesprávný formát argumentů\n");
            /* Nastavime priznak chyby (occurenceOfErrorInMainProcess = true) */
            occurenceOfErrorInMainProcess = true;
            break;

        /* Pokud doslo k chybe pri operaci fork() */
        case FORK_OPERATION_FAILED:
            /* Zapiseme chybu na chybovy vystup */
            fprintf(stderr, "Vyskytla se chyba pri operaci fork()\n");
            /* Nastavime priznak chyby (*occurenceOfError = true), cimz se ukonci generovani novych procesu */
            *occurenceOfError = true;
            /* Nakonec zvysime hodnoty vsech semaforu tak, aby jimi mohly bezpecne projit vsechny aktivni procesy.
             * Vyhoda je v tom, ze podprocesy tak prirozene po sobe uklidi zdroje a ukonci se */
            int i = 0;
            while (i < NUMBER_OF_SEMAPHORES) {
                for (int j = 0; j <= numberOfGeneratedPersons; j++) {
                    sem_post(arrayOfSemaphores[i]);
                }
                i++;
            }
            break;

        /* Pokud doslo k chybe pri operaci fopen() */
        case FOPEN_OPERATION_FAILED:
            fprintf(stderr, "Vyskytla se chyba pri operaci fopen()\n");
            occurenceOfErrorInMainProcess = true;
            break;

        /* Pokud doslo k chybe pri operaci mmap() */
        case MMAP_OPERATION_FAILED:
            fprintf(stderr, "Vyskytla se chyba pri operaci mmap()\n");
            occurenceOfErrorInMainProcess = true;
            break;

        /* Pokud doslo k chybe pri operaci sem_open() */
        case SEM_OPEN_OPERATION_FAILED:
            fprintf(stderr, "Vyskytla se chyba pri operaci sem_open()\n");
            occurenceOfErrorInMainProcess = true;
            break;
    }

    return;
}

bool StringToNumber(char *stringArgument,int *resultOfConversion) {

    /* Ukazatel na posledni znak v retezci, ktery nelze prevest na cislo */
    char *endptr;
    /* Do promenne, jejiz adresa byla predana jako druhy parametr, ulozime vysledek konverze */
    *resultOfConversion = strtol(stringArgument, &endptr, 10);
    /* Pokud neni hodnota posledniho znaku, ktery nemohl byt preveden na cislo rovna 0,
     * tak retezec nereprezentoval cislo a vratime false */
    if (*endptr != '\0') {
        return false;
    }

    return true;
}

void AllocateCommonResources() {

    /* Otevreme soubor a ulozime si popisovac na tento otevreny soubor, take si hlidame jestli se operace zdarila */
    soubor = fopen("rivercrossing.out", "w");
    if (soubor == NULL) {
        HandleError(FOPEN_OPERATION_FAILED);
        return;
    }

    /* Nyni budeme alokovat sdilenou pamet, mapovat do ni sdilene promenne a struktury a prirazovat jim vychozi hodnoty */
    /* Vytvarime sdileny pametovy objekt */
    void *mmapResult;
    int occurenceOfErrorDescriptor = shm_open("/occurenceOfErrorDescriptor_xherec00", O_RDWR | O_CREAT | O_TRUNC, 0644);
    /* Vytvorenemu objektu priradime potrebnou velikost */
    ftruncate(occurenceOfErrorDescriptor, sizeof(bool));
    /* Nakonec na tento objekt namapujeme na promennou */
    mmapResult = mmap(NULL, sizeof(bool), PROT_WRITE | PROT_READ, MAP_SHARED, occurenceOfErrorDescriptor, 0);
    /* Hlidame si, jestli se operace zdarila, toto staci hlidat jen u mmap(), i kdyby doslo k chybe pri shm_open(),
     * nebo ftruncate(), pozname to prave zde, protoze dojde k chybe pri mmap(), ktera na predchozich dvou zavisla */
    if (mmapResult == MAP_FAILED) {
        HandleError(MMAP_OPERATION_FAILED);
        return;
    }
    /* Pokud se operace zdarila, muzeme provest mapovani primo na sdilenou promennou */
    occurenceOfError = (bool *) mmapResult;
    /* Ktere take priradime vychozi hodnotu */
    *occurenceOfError = false;

    /* Stejna posloupnost akci, jako u vyse uvedeneho kodu se opakuje pro mapovani dalsich sdilenych promennych */
    int sharedVarsDescriptor = shm_open("/sharedVarsDescriptor_xherec00", O_RDWR | O_CREAT | O_TRUNC, 0644);
    ftruncate(sharedVarsDescriptor, sizeof(SharedVars));
    mmapResult = mmap(NULL, sizeof(SharedVars), PROT_WRITE | PROT_READ, MAP_SHARED, sharedVarsDescriptor, 0);
    if (mmapResult == MAP_FAILED) {
        HandleError(MMAP_OPERATION_FAILED);
        return;
    }
    sharedVars = (SharedVars *) mmapResult;
    sharedVars->counterOfActions = 0;
    sharedVars->counterOfHackers = 0;
    sharedVars->counterOfSerfs = 0;
    sharedVars->numberOfHackersOnPier = 0;
    sharedVars->numberOfSerfsOnPier = 0;
    sharedVars->numberOfHackersOnBoat = 0;
    sharedVars->numberOfSerfsOnBoat = 0;
    sharedVars->numberOfPersonsWhoPrintedTheirPositions = 0;
    sharedVars->numberOfPersonsWhoLanded = 0;

    /* Zde vytvarime pojmenovane semafory */
    semAllowsWritingToFileAndIncrementActions = sem_open("/semAllowsWritingToFileAndIncrementActions_xherec00", O_CREAT | O_EXCL | O_EXCL, 0644, 1);
    semAllowsAccessOnCoast = sem_open("/semAllowsAccessOnCoast_xherec00", O_CREAT | O_EXCL, 0644, 1);
    semAllowsAccessOnPier = sem_open("/semAllowsAccessOnPier_xherec00", O_CREAT | O_EXCL, 0644, 1);
    semAllowsBoarding = sem_open("/semAllowsBoarding_xherec00", O_CREAT | O_EXCL, 0644, 1);
    semAllowsBoardingForHacker = sem_open("/semAllowsBoardingForHacker_xherec00", O_CREAT | O_EXCL, 0644, 0);
    semAllowsBoardingForSerf = sem_open("/semAllowsBoardingForSerf_xherec00", O_CREAT | O_EXCL, 0644, 0);
    semAllowsPrintPositionOnBoat = sem_open("/semAllowsPrintPositionOnBoat_xherec00", O_CREAT | O_EXCL, 0644, 0);
    semAllowsVoyage = sem_open("/semAllowsVoyage_xherec00", O_CREAT | O_EXCL, 0644, 0);
    semAllowsLanding = sem_open("/semAllowsLanding_xherec00", O_CREAT | O_EXCL, 0644, 0);
    semAllowsToFinish = sem_open("/semAllowsToFinish_xherec00", O_CREAT | O_EXCL, 0644, 0);

    /* A nasledne jednotlive semafory priradime do pole, ktere seskupuje vsechny semafory,
     * respektive seskupuje ukazatele na tyto semafory */
    arrayOfSemaphores[0] = semAllowsWritingToFileAndIncrementActions;
    arrayOfSemaphores[1] = semAllowsAccessOnCoast;
    arrayOfSemaphores[2] = semAllowsAccessOnPier;
    arrayOfSemaphores[3] = semAllowsBoarding;
    arrayOfSemaphores[4] = semAllowsBoardingForHacker;
    arrayOfSemaphores[5] = semAllowsBoardingForSerf;
    arrayOfSemaphores[6] = semAllowsPrintPositionOnBoat;
    arrayOfSemaphores[7] = semAllowsVoyage;
    arrayOfSemaphores[8] = semAllowsLanding;
    arrayOfSemaphores[9] = semAllowsToFinish;

    /* Overime, jestli neselhalo vytvoreni nektereho ze semaforu */
    for (int i = 0; i < NUMBER_OF_SEMAPHORES; i++) {
        if (arrayOfSemaphores[i] == SEM_FAILED) {
            HandleError(SEM_OPEN_OPERATION_FAILED);
            break;
        }
    }

    return;
}

void GenerateDescendantsOfMainProcess() {

    pid_t pid;
    pid_t pid3;

    /* Vytvarime prvniho potomka */
    pid = fork();
    /* Pokud nelze vytvorit prvniho potomka, vyvolame chybu */
    if (pid == -1 && *occurenceOfError == false) {
        HandleError(FORK_OPERATION_FAILED);
    }
    /* Pokud se jedna o prvniho potomka */
    else if (pid == 0) {
        /* Prepneme se funkce, kde prvni potomek bude generovat dalsi potomky (hackery) */
        BeginProcessForGeneratingHackers();
        /* Uklidime zdroje, ktere prislusi tomuto procesu */
        CleanUpResourcesForActualProcess();
        /* A ukoncime prvniho potomka */
        exit (0);
    }
    /* Pokud se jedna o hlavni proces */
    else {
        /* Vytvarime druheho potomka */
        pid = fork();
        /* Pokud nelze vytvorit druheho potomka, vyvolame chybu */
        if (pid == -1 && *occurenceOfError == false) {
            HandleError(FORK_OPERATION_FAILED);
        }
        /* Pokud se jedna o druheho potomka */
        else if (pid == 0) {
            /* Prepneme se funkce, kde druhy potomek bude generovat dalsi potomky (serfy) */
            BeginProcessForGeneratingSerfs();
            /* Uklidime zdroje, ktere prislusi tomuto procesu */
            CleanUpResourcesForActualProcess();
            /* A ukoncime druheho potomka */
            exit (0);
        }
        /* Pokud se jedna o hlavni proces */
        else {
            /* Pockame na ukonceni vsech potomku */
            while ((pid3 = waitpid(-1, NULL, 0)) != 0) {
                if (errno == ECHILD)
                    break;
            }
        }
    }

    return;
}

void BeginProcessForGeneratingHackers() {

    pid_t pid2;
    pid_t pid3;

    /* Prvni potomek hlavniho procesu generuje dalsi procesy (hackery) */
    for (int i = 0; i < numberOfGeneratedPersons; i++) {
        /* Pokud vznikla nejaka chyba (zrejme pri systemovem volani), ukoncime generovani hackeru */
        if (*occurenceOfError == true) {
            break;
        }
        /* Nastavime seminko pro pseudonahodne generovani cisel */
        srand(time(NULL));
        /* Uspime proces a tedy na nejaky cas pozastavime generovani hackeru */
        usleep(rand() % ((maxTimeAfterNewHackerIsGenerated * 1000) + 1));
        /* Vytvarime noveho hackera */
        pid2 = fork();
        /* Pokud nelze vytvorit noveho hackera, vyvolame chybu */
        if (pid2 == -1 && *occurenceOfError == false) {
            HandleError(FORK_OPERATION_FAILED);
        }
        /* Pokud se jedna o hackera, vyskocime z cyklu */
        else if (pid2 == 0) {
            break;
        }
    }
    /* Pokud se jedna o hackera */
    if (pid2 == 0) {
        /* Hacker prichazi na breh */
        Started(HACKER);
        /* Hacker se pokousi vstoupit na molo */
        WaitingForBoarding(HACKER);
        /* Hacker se pokousi nalodit, pocet hackeru, kteri se mohou nyni nalodit reguluje semafor semAllowsBoardingForHacker */
        sem_wait(semAllowsBoardingForHacker);
        Boarding(HACKER);
        /* Po nalodeni tiskne hacker svoji pozici na lodi */
        PrintPositionOfPersonOnTheBoat(HACKER);
        /* Dokud posledni osoba nevytiskne svou pozici, budou na semaforu semAllowsVoyage cekat zbyli hackeri (ale i serfove) na lodi */
        sem_wait(semAllowsVoyage);
        /* Pokud se jedna o kapitana, uspi se proces (coz simuluje plavbu) na urcity cas a po probuzeni, tedy dokonceni plavby,
         * je semafor semAllowsLanding inkrementovan a je tedy povoleno vylodeni clenu posadky */
        if (positionOfPersonOnTheBoat == CAPTAIN) {
            /* Nastavime seminko pro pseudonahodne generovani cisel */
            srand(time(NULL));
            usleep(rand() % ((maxTimeOfVoyage * 1000) + 1));
            sem_post(semAllowsLanding);
        }
        /* Hacker se pokousi vylodit, to se mu podari, jakmile je dokoncena plavba */
        Landing(HACKER);
        /* Hacker se pokousi odejit ze sceny, to se mu podari,
         * jakmile vsechny osoby absolvovali projizdku na lodi a vylodili se */
        Finished(HACKER);
        /* Ale jeste, nez hacker skutecne zmizi ze sceny, je natolik slusny, ze po sobe uklidi zdroje */
        CleanUpResourcesForActualProcess();
        /* A ted uz neni po hackerovi ani stopy */
        exit (0);
    }
    /* Pokud se jedna o prvniho potomka hlavniho procesu */
    else {
        /* Prvni potomek hl. procesu pocka na ukonceni vsech jeho potomku (hackeru) */
        while ((pid3 = waitpid(-1, NULL, 0)) != 0) {
            if (errno == ECHILD)
                break;
        }
    }

    return;
}

void BeginProcessForGeneratingSerfs() {

    pid_t pid2;
    pid_t pid3;

    /* Druhy potomek hlavniho procesu generuje dalsi procesy (serfy) */
    for (int j = 0; j < numberOfGeneratedPersons; j++) {
        /* Pokud vznikla nejaka chyba (zrejme pri systemovem volani), ukoncime generovani serfu */
        if (*occurenceOfError == true) {
            break;
        }
        /* Nastavime seminko pro pseudonahodne generovani cisel */
        srand(time(NULL));
        /* Uspime proces a tedy na nejaky cas pozastavime generovani serfu */
        usleep(rand() % ((maxTimeAfterNewSerfIsGenerated * 1000) + 1));
        /* Vytvarime noveho serfa */
        pid2 = fork();
        /* Pokud nelze vytvorit noveho serfa, vyvolame chybu */
        if (pid2 == -1 && *occurenceOfError == false) {
            HandleError(FORK_OPERATION_FAILED);
        }
        /* Pokud se jedna o serfa, vyskocime z cyklu */
        else if (pid2 == 0) {
            break;
        }
    }
    /* Pokud se jedna o serfa */
    if (pid2 == 0) {
        /* Serf prichazi na breh */
        Started(SERF);
        /* Serf se pokousi vstoupit na molo */
        WaitingForBoarding(SERF);
        /* Serf se pokousi nalodit, pocet serfu, kteri se mohou nyni nalodit reguluje semafor semAllowsBoardingForSerf */
        sem_wait(semAllowsBoardingForSerf);
        Boarding(SERF);
        /* Po nalodeni tiskne serf svoji pozici na lodi */
        PrintPositionOfPersonOnTheBoat(SERF);
        /* Dokud posledni osoba nevytiskne svou pozici, budou na semaforu semAllowsVoyage cekat zbyli serfove (ale i hackeri) na lodi */
        sem_wait(semAllowsVoyage);
        /* Pokud se jedna o kapitana, uspi se proces (coz simuluje plavbu) na urcity cas a po probuzeni, tedy dokonceni plavby,
         * je semafor semAllowsLanding inkrementovan a je tedy povoleno vylodeni clenu posadky */
        if (positionOfPersonOnTheBoat == CAPTAIN) {
            usleep(rand() % ((maxTimeOfVoyage * 1000) + 1));
            sem_post(semAllowsLanding);
        }
        /* Serf se pokousi vylodit, to se mu podari, jakmile je dokoncena plavba */
        Landing(SERF);
        /* Serf se pokousi odejit ze sceny, to se mu podari,
         * jakmile vsechny osoby absolvovali projizdku na lodi a vylodili se */
        Finished(SERF);
        /* Ale jeste, nez serf skutecne zmizi ze sceny, je natolik slusny, ze po sobe uklidi zdroje */
        CleanUpResourcesForActualProcess();
        /* A ted uz neni po serfovi ani stopy */
        exit (0);
    }
    /* Pokud se jedna o druheho potomka hlavniho procesu */
    else {
        /* Druhy potomek hl. procesu pocka na ukonceni vsech jeho potomku (serfu) */
        while ((pid3 = waitpid(-1, NULL, 0)) != 0) {
            if (errno == ECHILD)
                break;
        }
    }

    return;
}

void BasicSynchronizationFunction() {

    /* Viz. popis funkce u deklarace prototypu funkce */
    sem_wait(semAllowsWritingToFileAndIncrementActions);
    sharedVars->counterOfActions++;

    return;
}

void Started(CategoryOfPerson categoryOfPerson) {

    /* Semafor semAllowsAccessOnCoast zajistuje, ze osoby budou prichazet na breh jedna po druhe,
     * povazuji totiz akci prijiti na breh (kterou reprezentuje tato funkce) za atomickou */
    sem_wait(semAllowsAccessOnCoast);
    switch(categoryOfPerson) {
        /* Na breh prichazi hacker */
        case HACKER:
            /* BasicSynchronizationFunction() zajisti atomicitu inkrementace citace akci
             * a taky nasledujich prikazu v teto vetvi case */
            BasicSynchronizationFunction();
            /* Inkrementujeme citac hackeru */
            sharedVars->counterOfHackers++;
            /* Aktualni hodnotu citace  hackeru pouzijeme jako identifikator pro tohoto hackera */
            idOfPerson = sharedVars->counterOfHackers;
            /* Zapiseme do souboru provadenou akci, do souboru zapisujeme vylucne */
            fprintf(soubor, "%d: hacker: %d: started\n",sharedVars->counterOfActions,idOfPerson);
            fflush(soubor);
            /* Konec atomicity pro tuto vetev case */
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;

        /* Na breh prichazi serf */
        case SERF:
            /* BasicSynchronizationFunction() zajisti atomicitu inkrementace citace akci
             * a taky nasledujich prikazu v teto vetvi case */
            BasicSynchronizationFunction();
            /* Inkrementujeme citac serfu */
            sharedVars->counterOfSerfs++;
            /* Aktualni hodnotu citace  serfu pouzijeme jako identifikator pro tohoto serfa */
            idOfPerson = sharedVars->counterOfSerfs;
            /* Zapiseme do souboru provadenou akci, do souboru zapisujeme vylucne */
            fprintf(soubor,"%d: serf: %d: started\n",sharedVars->counterOfActions,idOfPerson);
            fflush(soubor);
            /* Konec atomicity pro tuto vetev case*/
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;
    }
    /* Konec atomicity pro akci "vstup osoby na breh" */
    sem_post(semAllowsAccessOnCoast);

    return;
}

void WaitingForBoarding(CategoryOfPerson categoryOfPerson) {

    /* Semafor semAllowsAccessOnPier zajistuje, ze osoby budou vstupovat na molo postupne a pri vstupu na molo budou regulovany,
     * vstup jim tak muze byt pro urcity okamzik povolen, nebo zamitnut */
    sem_wait(semAllowsAccessOnPier);
    switch(categoryOfPerson) {
        case HACKER:
            BasicSynchronizationFunction();
            /* Inkrementace citace hackeru na molu */
            sharedVars->numberOfHackersOnPier++;
            fprintf(soubor, "%d: hacker: %d: waiting for boarding: %d: %d\n",sharedVars->counterOfActions,idOfPerson,
                    sharedVars->numberOfHackersOnPier, sharedVars->numberOfSerfsOnPier);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;

        case SERF:
            BasicSynchronizationFunction();
            /* Inkrementace citace serfu na molu */
            sharedVars->numberOfSerfsOnPier++;
            fprintf(soubor,"%d: serf: %d: waiting for boarding: %d: %d\n",sharedVars->counterOfActions,idOfPerson,
                    sharedVars->numberOfHackersOnPier, sharedVars->numberOfSerfsOnPier);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;
    }
    /* Pokud na molu existuje dvojice hackeru a dvojice serfu */
    if (sharedVars->numberOfHackersOnPier >= 2 && sharedVars->numberOfSerfsOnPier >= 2) {
        /* Snizime pocty hackeru a serfu na molu o tyto dvojice */
        sharedVars->numberOfHackersOnPier -= 2;
        sharedVars->numberOfSerfsOnPier -= 2;

        /* Tento proces se zaslouzil o to, aby mohlo probehnout nalodeni a tak ma pravo byt kapitanem */
        positionOfPersonOnTheBoat = CAPTAIN;
        /* Povolime pomoci semaforu dvojici hackeru a serfu vstup na lod */
        sem_post(semAllowsBoardingForHacker);
        sem_post(semAllowsBoardingForSerf);
        sem_post(semAllowsBoardingForHacker);
        sem_post(semAllowsBoardingForSerf);
    }
    /* Pokud na molu neexistuje dvojice hackeru a dvojice serfu, ale je mozne utvorit skupinku ze ctyr hackeru */
    else if (sharedVars->numberOfHackersOnPier >= 4 && sharedVars->numberOfSerfsOnPier < 2) {
        /* Snizime pocty hackeru na molu o tuto ctverici */
        sharedVars->numberOfHackersOnPier -= 4;
        /* Tento proces se zaslouzil o to, aby mohlo probehnout nalodeni a tak ma pravo byt kapitanem */
        positionOfPersonOnTheBoat = CAPTAIN;
        /* Povolime pomoci semaforu ctverici hackeru vstup na lod */
        sem_post(semAllowsBoardingForHacker);
        sem_post(semAllowsBoardingForHacker);
        sem_post(semAllowsBoardingForHacker);
        sem_post(semAllowsBoardingForHacker);
    }
    /* Pokud na molu neexistuje dvojice hackeru a dvojice serfu, a neni mozne utvorit skupinku ze ctyr hackeru,
     * ale je mozne utvorit ctverici serfu */
    else if (sharedVars->numberOfSerfsOnPier >= 4 && sharedVars->numberOfHackersOnPier < 2) {
        /* Snizime pocty serfu na molu o tuto ctverici */
        sharedVars->numberOfSerfsOnPier -= 4;
        /* Tento proces se zaslouzil o to, aby mohlo probehnout nalodeni a tak ma pravo byt kapitanem */
        positionOfPersonOnTheBoat = CAPTAIN;
        /* Povolime pomoci semaforu ctverici serfu vstup na lod */
        sem_post(semAllowsBoardingForSerf);
        sem_post(semAllowsBoardingForSerf);
        sem_post(semAllowsBoardingForSerf);
        sem_post(semAllowsBoardingForSerf);
    }
    /* Pokud neni mozne vytvorit odpovidajici skupinku osob, neni mozne priradit urcitou pozici teto osobe (procesu),
     * a pomoci inkrementace semaforu semAllowsAccessOnPier umoznime vstup na molo dalsi osobe */
    else {
        positionOfPersonOnTheBoat = UNKNOWN;
        sem_post(semAllowsAccessOnPier);
    }

    return;
}

void Boarding(CategoryOfPerson categoryOfPerson) {

    /* Semafor semAllowsBoarding zajistuje, ze osoby se budou nalodovat */
    sem_wait(semAllowsBoarding);
    switch(categoryOfPerson) {
        case HACKER:
            /* Inkrementace citace hackeru na lodi */
            sharedVars->numberOfHackersOnBoat++;
            BasicSynchronizationFunction();
            fprintf(soubor, "%d: hacker: %d: boarding: %d: %d\n",sharedVars->counterOfActions,idOfPerson,
                    sharedVars->numberOfHackersOnPier, sharedVars->numberOfSerfsOnPier);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;

        case SERF:
            /* Inkrementace citace serfu na lodi */
            sharedVars->numberOfSerfsOnBoat++;
            BasicSynchronizationFunction();
            fprintf(soubor, "%d: serf: %d: boarding: %d: %d\n",sharedVars->counterOfActions,idOfPerson,
                    sharedVars->numberOfHackersOnPier, sharedVars->numberOfSerfsOnPier);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;
    }
    /* Pokud je osoba na lodi, je potreba ji priradit pozici, pokud se nejedna o kapitana, tak dostane pozici MEMBER */
    if (positionOfPersonOnTheBoat == UNKNOWN) {
        positionOfPersonOnTheBoat = MEMBER;
    }

    /* Je uz lod plne obsazena a pripravena k vypluti? Tedy overujeme, jestli se na ni vyskytuje vhodna skupinka osob
     * Pokud ano, tak inkrementaci semaforu semAllowsPrintPositionOnBoat umoznime posadce tisknout svoji pozici na lodi */
    if ((sharedVars->numberOfHackersOnBoat == 2 && sharedVars->numberOfSerfsOnBoat == 2)
        || (sharedVars->numberOfHackersOnBoat == 4 && sharedVars->numberOfSerfsOnBoat == 0)
        || (sharedVars->numberOfHackersOnBoat == 0 && sharedVars->numberOfSerfsOnBoat == 4)) {
        sem_post(semAllowsPrintPositionOnBoat);
    }
    /* Umoznime se nalodit dalsi osobe, pokud takova osoba existuje */
    sem_post(semAllowsBoarding);

    return;
}

void PrintPositionOfPersonOnTheBoat(CategoryOfPerson categoryOfPerson) {

    /* Semafor semAllowsPrintPositionOnBoat zajistuje, ze osoby budou tisknout svou pozici na lodi postupne a taky reguluje tuto akci,
     * tedy dokud nejsou vsichni nalodeni, tak tato akce neni povolena */
    sem_wait(semAllowsPrintPositionOnBoat);
    switch(categoryOfPerson) {
        case HACKER:
            if (positionOfPersonOnTheBoat == MEMBER) {
                BasicSynchronizationFunction();
                fprintf(soubor,"%d: hacker: %d: member\n",sharedVars->counterOfActions,idOfPerson);
                fflush(soubor);
                sem_post(semAllowsWritingToFileAndIncrementActions);
            }
            else {
                BasicSynchronizationFunction();
                fprintf(soubor,"%d: hacker: %d: captain\n",sharedVars->counterOfActions,idOfPerson);
                fflush(soubor);
                sem_post(semAllowsWritingToFileAndIncrementActions);
            }
            break;

        case SERF:
            if (positionOfPersonOnTheBoat == MEMBER) {
                BasicSynchronizationFunction();
                fprintf(soubor,"%d: serf: %d: member\n",sharedVars->counterOfActions,idOfPerson);
                fflush(soubor);
                sem_post(semAllowsWritingToFileAndIncrementActions);
            }
            else {
                BasicSynchronizationFunction();
                fprintf(soubor, "%d: serf: %d: captain\n",sharedVars->counterOfActions,idOfPerson);
                fflush(soubor);
                sem_post(semAllowsWritingToFileAndIncrementActions);
            }
            break;
    }
    /* Inkrementujeme citac, ktery udava kolik osob na lodi zatim vytisklo svuj status */
    sharedVars->numberOfPersonsWhoPrintedTheirPositions++;
    /* Pokud uz vsechny osoby vytiskli svou pozici na lodi */
    if (sharedVars->numberOfPersonsWhoPrintedTheirPositions == 4) {
        /* Vynulujeme citac, udavajici kolik osob vytisklo svuj status na lodi */
        sharedVars->numberOfPersonsWhoPrintedTheirPositions = 0;
        /* A umoznime pomoci nekolikanasobne inkrementace semaforu prejit k samotne plavbe */
        sem_post(semAllowsVoyage);
        sem_post(semAllowsVoyage);
        sem_post(semAllowsVoyage);
        sem_post(semAllowsVoyage);
    }
    /* Pokud vsechny osoby zatim nevytiskly svou pozici na lodi, umoznime dalsi osobe vytisknout svou pozici na lodi,
     * tedy pokud takova osoba existuje */
    else {
        sem_post(semAllowsPrintPositionOnBoat);
    }

    return;
}

void Landing(CategoryOfPerson categoryOfPerson) {

    /* Semafor semAllowsLanding zajistuje, ze osoby se budou vylodovat postupne a taky reguluje tuto akci,
     * tedy dokud neni ukoncena plavba neni povoleno ano vylodeni */
    sem_wait(semAllowsLanding);
    switch(categoryOfPerson) {
        case HACKER:
            BasicSynchronizationFunction();
            fprintf(soubor, "%d: hacker: %d: landing: %d: %d\n",sharedVars->counterOfActions,idOfPerson,
                    sharedVars->numberOfHackersOnPier, sharedVars->numberOfSerfsOnPier);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;

        case SERF:
            BasicSynchronizationFunction();
            fprintf(soubor, "%d: serf: %d: landing: %d: %d\n",sharedVars->counterOfActions,idOfPerson,
                    sharedVars->numberOfHackersOnPier, sharedVars->numberOfSerfsOnPier);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;
    }
    /* Inkrementujeme citac, ktery udava kolik osob se zatim vylodilo */
    sharedVars->numberOfPersonsWhoLanded++;
    /* Pokud se uz vsechny osoby vylodili */
    if (sharedVars->numberOfPersonsWhoLanded == 4) {
        /* Vynulujeme citac, udavajici kolik osob se vylodilo */
        sharedVars->numberOfPersonsWhoLanded = 0;
        /* Vynulujeme citac osob (serfu a hackeru) na lodi */
        sharedVars->numberOfHackersOnBoat = 0;
        sharedVars->numberOfSerfsOnBoat = 0;
        /* Umoznime vstoupit dalsim osobam na molo */
        sem_post(semAllowsAccessOnPier);
        /* Pokud se jedna o posledni osobu (proces), ktera vystoupila z lodi
         * (coz pozname podle hodnoty citace akci, ktera se rovna poctu planovanych osob k vygenerovani osob vynasobeneho poctem jejich akci (5)),
         * tak inkrementujeme semafor semAllowsToFinish, cimz umoznime vsem osobam odejit ze sceny */
        if (sharedVars->counterOfActions == ((numberOfGeneratedPersons * 2) * 5)) {
           sem_post(semAllowsToFinish);
        }
    }
    /* Pokud vsechny osoby zatim nevystoupili z lodi, umoznime dalsi osobe se vylodit,
     * tedy pokud takova osoba existuje */
    else {
        sem_post(semAllowsLanding);
    }

    return;
}

void Finished(CategoryOfPerson categoryOfPerson) {

    /* Semafor semAllowsToFinish zajistuje, ze osoby budou opoustet scenu postupne a taky reguluje tuto akci,
     * tedy dokud nejsou vsechny osoby vylodeny, neni povoleno ani opusteni sceny */
    sem_wait(semAllowsToFinish);
    switch(categoryOfPerson) {
        case HACKER:
            BasicSynchronizationFunction();
            fprintf(soubor, "%d: hacker: %d: finished\n",sharedVars->counterOfActions,idOfPerson);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;

        case SERF:
            BasicSynchronizationFunction();
            fprintf(soubor, "%d: serf: %d: finished\n",sharedVars->counterOfActions,idOfPerson);
            fflush(soubor);
            sem_post(semAllowsWritingToFileAndIncrementActions);
            break;
    }
    /* Konec atomicity opusteni sceny soucasnym procesem */
    sem_post(semAllowsToFinish);

    return;
}

void CleanUpResourcesForActualProcess() {

    /* Uvolnime semafory, ktere patri soucasnemu procesu */
    sem_close(semAllowsWritingToFileAndIncrementActions);
    sem_close(semAllowsAccessOnCoast);
    sem_close(semAllowsAccessOnPier);
    sem_close(semAllowsBoarding);
    sem_close(semAllowsBoardingForHacker);
    sem_close(semAllowsBoardingForSerf);
    sem_close(semAllowsPrintPositionOnBoat);
    sem_close(semAllowsVoyage);
    sem_close(semAllowsLanding);
    sem_close(semAllowsToFinish);

    return;
}

void CleanUpCommonResources() {

    /* Zavreme popisovac souboru */
    fclose(soubor);

    /* Odstranime pojmenovane semafory ze systemu */
    sem_unlink("/semAllowsWritingToFileAndIncrementActions_xherec00");
    sem_unlink("/semAllowsAccessOnCoast_xherec00");
    sem_unlink("/semAllowsAccessOnPier_xherec00");
    sem_unlink("/semAllowsBoarding_xherec00");
    sem_unlink("/semAllowsBoardingForHacker_xherec00");
    sem_unlink("/semAllowsBoardingForSerf_xherec00");
    sem_unlink("/semAllowsPrintPositionOnBoat_xherec00");
    sem_unlink("/semAllowsVoyage_xherec00");
    sem_unlink("/semAllowsLanding_xherec00");
    sem_unlink("/semAllowsToFinish_xherec00");

    /* Odstranime sdilene promenne ze systemu a uvolnime pamet, do ktere byly namapovany */
    munmap(sharedVars, sizeof(SharedVars));
    shm_unlink("/sharedVarsDescriptor_xherec00");

    munmap(occurenceOfError, sizeof(bool));
    shm_unlink("/occurenceOfErrorDescriptor_xherec00");

    return;
}


