/**
 * Soubor:                 rivercrossing.h
 * Datum vytvoreni:        13. 4. 2014
 * Datum posledni upravy:  21. 4. 2014
 * Autor:                  Jan Herec, xherec00@stud.fit.vutbr.cz
 * Projekt:                River Crossing Problem, projekt cislo 2 do predmetu iOS na FIT VUT
 * Popis programu:         Program resi synchronizacni problem: River Crossing Problem
 */

#define NUMBER_OF_SEMAPHORES 10

/* Definujeme vycet, ktery obsahuje typy chybovych stavu */
typedef enum {
    INCORRECT_FORMAT_OF_ARGUMENTS,
    FORK_OPERATION_FAILED,
    MMAP_OPERATION_FAILED,
    FOPEN_OPERATION_FAILED,
    SEM_OPEN_OPERATION_FAILED
} TypeOfError;

/* Definujeme vycet, ktery obsahuje kategorie osob */
typedef enum {
    HACKER,
    SERF
} CategoryOfPerson;

/* Definujeme vycet, ktery obsahuje pozice posadky na lodi */
typedef enum {
    CAPTAIN,
    MEMBER,
    UNKNOWN
} PositionOfPersonOnTheBoat;

/* Definujeme semafory, ktere budeme pouzivat pro synchronizaci */
sem_t *semAllowsWritingToFileAndIncrementActions;   // Semafor povolujici zapisovani do souboru a Inkrementaci akci
sem_t *semAllowsAccessOnCoast;                      // Semafor povolujici pristup na pobrezi
sem_t *semAllowsAccessOnPier;                       // Semafor povolujici pristup na molo
sem_t *semAllowsBoarding;                           // Semafor povolujici nalodeni
sem_t *semAllowsBoardingForHacker;                  // Semafor povolujici nalodeni pro hackera
sem_t *semAllowsBoardingForSerf;                    // Semafor povolujici nalodeni pro serfa
sem_t *semAllowsPrintPositionOnBoat;                // Semafor povolujici zapisovani pozice na lodi
sem_t *semAllowsVoyage;                             // Semafor povolujici plavbu
sem_t *semAllowsLanding;                            // Semafor povolujici vylodeni
sem_t *semAllowsToFinish;                           // Semafor povolujici ukonceni

/* Vytvarime pole, které pozdeji naplnime semafory, budeme tak moci pres jednotlive semafory iterovat */
sem_t *arrayOfSemaphores[NUMBER_OF_SEMAPHORES];

/* Struktura, ktera obsahuje sdilene promenne, ktere jsou ve sdilene pameti */
typedef struct {
    int counterOfActions;                          // Citac akci
    int counterOfHackers;                          // Citac hackeru
    int counterOfSerfs;                            // Citac serfu
    int numberOfHackersOnPier;                     // Hackerů na molu
    int numberOfSerfsOnPier;                       // Serfů na molu
    int numberOfHackersOnBoat;                     // Hackerů na lodi
    int numberOfSerfsOnBoat;                       // Serfů na lodi
    int numberOfPersonsWhoPrintedTheirPositions;   // Kolik clenu ze soucasne posadky zatim vytisklo svuj status na lodi
    int numberOfPersonsWhoLanded;                  // Kolik osob ze soucasne posadky lodi se zatim vylodilo
} SharedVars;

/* Identifikátor osoby, ktery je osobe prirazen po prichodu na breh a je unikatni jen v ramci dane kategorie osob */
int idOfPerson = 0;

/* Promenna, ktera udava, jestli nastala pri behu programu chyba */
bool *occurenceOfError = NULL;

/* Promenna, ktera udava, jestli nastala pri behu programu chyba, tentokrat je vsak hlidan vznik chyby jen v hlavnim procesu */
bool occurenceOfErrorInMainProcess = false;

/* Pocet osob, ktere jsou generovany v kazde kategorii */
int numberOfGeneratedPersons = 0;

/* Maximalni cas v milisekundach, po kterem je generovan novy hacker */
int maxTimeAfterNewHackerIsGenerated = 0;

/* Maximalni cas v milisekundach, po kterem je generovan novy serf */
int maxTimeAfterNewSerfIsGenerated = 0;

/* Maximalni cas v milisekundach, po ktery trva plavba */
int maxTimeOfVoyage = 0;

/**
 * Funkce anylyzuje a zpracuje argumenty predane programu
 * Pokud pri zpracovani nastane chyba, nastavi priznak occurenceOfError na true
 * @param argc Pocet argumentu predanych programu
 * @param argv Pole argumentu (retezcu) predanych programu
 */
void ParseArgumentsPassedToProgram(int argc, char** argv);

/**
 * Funkce prevadi retezec na cele cislo (int)
 * @param stringArgument Retezec, ktery budeme prevadet na cislo
 * @param resultOfConversion Ukazatel na int, na misto kam ukazuje ulozime vysledek konverze
 * @return True nebo False v zavislosti na tom, zdali se podarilo retezec na cislo uspesne prevest
 */
bool StringToNumber(char *stringArgument, int *resultOfConversion);

/**
 * Funkce ve ktere zacina vytvareni podprocesu (potomku)
 */
void GenerateDescendantsOfMainProcess();

/**
 * Funkce predstavuje proces pro generovani hackers
 */
void BeginProcessForGeneratingHackers();

/**
 * Funkce predstavuje proces pro generovani serfs
 */
void BeginProcessForGeneratingSerfs();

/**
 * Funkce alokuje prostredky (semafory, sdilene promenne, popisovac souboru), ktere jsou spolecne pro vice procesu
 */
void AllocateCommonResources();

/**
 * Funkce uklidi prostredky (semafory, sdilene promenne, popisovac souboru), ktere jsou spolecne pro vice procesu
 */
void CleanUpCommonResources();

/**
 * Funkce uklidi prostredky (uzavre semafory), ktere jsou specificke pro dany proces
 */
void CleanUpResourcesForActualProcess();

/**
 * Funkce, jejiz zavolanim zajistujeme, ze se aktivuje semafor, ktery umozni atomicky inkrementovat hodnotu citace akci
 * a taky dovoli vylucny zapis provadene akce do souboru
 */
void BasicSynchronizationFunction();

/**
 * Funkce predstavuje prichod osoby na breh
 */
void Started(CategoryOfPerson categoryOfPerson);

/**
 * Funkce predstavuje prichod osoby na molo
 */
void WaitingForBoarding(CategoryOfPerson categoryOfPerson);

/**
 * Funkce predstavuje nalodeni osoby
 */
void Boarding(CategoryOfPerson categoryOfPerson);

/**
 * Funkce ve ktere dana osoba tiskne svoji pozici na lodi
 */
void PrintPositionOfPersonOnTheBoat(CategoryOfPerson categoryOfPerson);

/**
 * Funkce predstavuje pristani osoby
 */
void Landing(CategoryOfPerson categoryOfPerson);

/**
 * Funkce, kterou provede osoba pote, co uz vsechny osoby pristali na brehu
 */
void Finished(CategoryOfPerson categoryOfPerson);

/**
 * Funkce provadi obsluhu chyb
 * @param typeOfError Typ vznikle chyby
 */
void HandleError(TypeOfError typeOfError);

